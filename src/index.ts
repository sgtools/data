import * as fs from 'fs';

export interface DataItem
{
    id         ?: string;
    createdAt  ?: Date;    
}

export class Data
{
    private path: string;

    static plugins : string[];

    constructor()
    {
        this.path = '';

        Data.plugins = [];
    }

    public setPath(path: string)
    {
        this.path = path;
    }

    public static addPlugin(name: string)
    {
        Data.plugins.push(name);
    }

    public static CreateID()
    {
        // TODO CHECK ALREADY USED ?
        return Math.random().toString(16).substr(2, 14);
    }

    public static FilterItems<T extends DataItem>(plugin: string, filter: (item: T) => boolean): T[]
    {
        if (!data[plugin]) return undefined;

        return data[plugin].filter(filter);
    }

    public static FindItemById<T extends DataItem>(plugin: string, id: string): T
    {
        if (!data[plugin]) return undefined;

        return data[plugin].find((item: T) => item.id === id);
    }

    public static FindItem<T extends DataItem>(plugin: string, check: (item: T) => boolean): T
    {
        if (!data[plugin]) return undefined;

        return data[plugin].find(check);
    }

    public static FindItemIndex<T extends DataItem>(plugin: string, check: (item: T) => boolean): number
    {
        if (!data[plugin]) return undefined;

        return data[plugin].findIndex(check);
    }

    public static UpdateItem<T extends DataItem>(plugin: string, oldItem: T | string, updates: any): T
    {
        if (!data[plugin]) return undefined;

        let id = (typeof oldItem === 'string' ? oldItem : oldItem.id);

        let newItem = Data.FindItemById<T>(plugin, id);
        if (!newItem) return undefined;

        Object.keys(updates).forEach(key => {
            newItem[key] = updates[key];
        });
        newItem['updatedAt'] = new Date();

        let index = Data.FindItemIndex(plugin, item => item.id === id);
        Data.RemoveItem(plugin, index);

        data[plugin].push(newItem);
        data.save();
        return newItem;
   }

    public static SetItem<T extends DataItem>(plugin: string, content: T): void
    {
        if (!data[plugin]) return undefined;

        if (data[plugin].length !== 0) {
            Data.RemoveItem(plugin, 0);
        }
        Data.CreateItem(plugin, content);
    }

    public static CreateItem<T extends DataItem>(plugin: string, content: T): T
    {
        if (!data[plugin]) return undefined;

        let item = {
            id          : Data.CreateID(),
            createdAt   : new Date(),
            ...content
        };
        data[plugin].push(item);
        data.save();
        return item;
    }

    public static RemoveItem(plugin: string, index: number): boolean
    {
        if (!data[plugin]) return false;

        if (index >= 0 && index < data[plugin].length) {
            data[plugin].splice(index, 1);
            data.save();
            return true;
        }

        return false;
    }

    public load()
    {
        if (this.path === '') return false;

        if (fs.existsSync(this.path)) {
            let rawdata = fs.readFileSync(this.path, 'utf8');
            let content = JSON.parse(rawdata);

            Data.plugins.forEach(plugin => {
                if (Object.keys(content).includes(plugin)) {
                    this[plugin] = content[plugin];
                }
            });
            return true;
        }

        return false;
    }

    public save()
    {
        if (this.path === '') return false;

        let content = {};
        Data.plugins.forEach(plugin => {
            content[plugin] = this[plugin];
        });
        fs.writeFileSync(this.path, JSON.stringify(content));
        return true;
    }

}

export const data = new Data();