[@sgtools/data](../README.md) / [Exports](../modules.md) / Data

# Class: Data

## Hierarchy

* **Data**

## Table of contents

### Constructors

- [constructor](data.md#constructor)

### Properties

- [path](data.md#path)
- [plugins](data.md#plugins)

### Methods

- [load](data.md#load)
- [save](data.md#save)
- [setPath](data.md#setpath)
- [CreateID](data.md#createid)
- [CreateItem](data.md#createitem)
- [FilterItems](data.md#filteritems)
- [FindItem](data.md#finditem)
- [FindItemById](data.md#finditembyid)
- [FindItemIndex](data.md#finditemindex)
- [RemoveItem](data.md#removeitem)
- [SetItem](data.md#setitem)
- [UpdateItem](data.md#updateitem)
- [addPlugin](data.md#addplugin)

## Constructors

### constructor

\+ **new Data**(): [*Data*](data.md)

**Returns:** [*Data*](data.md)

Defined in: index.ts:13

## Properties

### path

• `Private` **path**: *string*

Defined in: index.ts:11

___

### plugins

▪ `Static` **plugins**: *string*[]

Defined in: index.ts:13

## Methods

### load

▸ **load**(): *boolean*

**Returns:** *boolean*

Defined in: index.ts:125

___

### save

▸ **save**(): *boolean*

**Returns:** *boolean*

Defined in: index.ts:144

___

### setPath

▸ **setPath**(`path`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`path` | *string* |

**Returns:** *void*

Defined in: index.ts:22

___

### CreateID

▸ `Static`**CreateID**(): *string*

**Returns:** *string*

Defined in: index.ts:32

___

### CreateItem

▸ `Static`**CreateItem**<T\>(`plugin`: *string*, `content`: T): T

#### Type parameters:

Name | Type |
------ | ------ |
`T` | [*DataItem*](../interfaces/dataitem.md) |

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`content` | T |

**Returns:** T

Defined in: index.ts:98

___

### FilterItems

▸ `Static`**FilterItems**<T\>(`plugin`: *string*, `filter`: (`item`: T) => *boolean*): T[]

#### Type parameters:

Name | Type |
------ | ------ |
`T` | [*DataItem*](../interfaces/dataitem.md) |

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`filter` | (`item`: T) => *boolean* |

**Returns:** T[]

Defined in: index.ts:38

___

### FindItem

▸ `Static`**FindItem**<T\>(`plugin`: *string*, `check`: (`item`: T) => *boolean*): T

#### Type parameters:

Name | Type |
------ | ------ |
`T` | [*DataItem*](../interfaces/dataitem.md) |

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`check` | (`item`: T) => *boolean* |

**Returns:** T

Defined in: index.ts:52

___

### FindItemById

▸ `Static`**FindItemById**<T\>(`plugin`: *string*, `id`: *string*): T

#### Type parameters:

Name | Type |
------ | ------ |
`T` | [*DataItem*](../interfaces/dataitem.md) |

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`id` | *string* |

**Returns:** T

Defined in: index.ts:45

___

### FindItemIndex

▸ `Static`**FindItemIndex**<T\>(`plugin`: *string*, `check`: (`item`: T) => *boolean*): *number*

#### Type parameters:

Name | Type |
------ | ------ |
`T` | [*DataItem*](../interfaces/dataitem.md) |

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`check` | (`item`: T) => *boolean* |

**Returns:** *number*

Defined in: index.ts:59

___

### RemoveItem

▸ `Static`**RemoveItem**(`plugin`: *string*, `index`: *number*): *boolean*

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`index` | *number* |

**Returns:** *boolean*

Defined in: index.ts:112

___

### SetItem

▸ `Static`**SetItem**<T\>(`plugin`: *string*, `content`: T): *void*

#### Type parameters:

Name | Type |
------ | ------ |
`T` | [*DataItem*](../interfaces/dataitem.md) |

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`content` | T |

**Returns:** *void*

Defined in: index.ts:88

___

### UpdateItem

▸ `Static`**UpdateItem**<T\>(`plugin`: *string*, `oldItem`: *string* \| T, `updates`: *any*): T

#### Type parameters:

Name | Type |
------ | ------ |
`T` | [*DataItem*](../interfaces/dataitem.md) |

#### Parameters:

Name | Type |
------ | ------ |
`plugin` | *string* |
`oldItem` | *string* \| T |
`updates` | *any* |

**Returns:** T

Defined in: index.ts:66

___

### addPlugin

▸ `Static`**addPlugin**(`name`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |

**Returns:** *void*

Defined in: index.ts:27
