@sgtools/data / [Exports](modules.md)

# Welcome to @sgtools/data
[![Version](https://npm.bmel.fr/-/badge/sgtools/data.svg)](https://npm.bmel.fr/-/web/detail/@sgtools/data)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/sgtools/data/-/blob/master/docs/README.md)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](https://spdx.org/licenses/ISC)

> Data handling for nodejs applications

## Install

```sh
npm install @sgtools/data
```

## Usage

```sh
import { data } from '@sgtools/data';
data.setPath('...');
```

Code documentation can be found [here](https://gitlab.com/sgtools/data/-/blob/master/docs/README.md).

## Author

**Sébastien GUERRI** <sebastien.guerri@apps.bmel.fr>

## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/data/issues). You can also contact the author.

## License

This project is [ISC](https://spdx.org/licenses/ISC) licensed.
