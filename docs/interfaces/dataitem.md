[@sgtools/data](../README.md) / [Exports](../modules.md) / DataItem

# Interface: DataItem

## Hierarchy

* **DataItem**

## Table of contents

### Properties

- [createdAt](dataitem.md#createdat)
- [id](dataitem.md#id)

## Properties

### createdAt

• `Optional` **createdAt**: Date

Defined in: index.ts:6

___

### id

• `Optional` **id**: *string*

Defined in: index.ts:5
