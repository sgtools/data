[@sgtools/data](README.md) / Exports

# @sgtools/data

## Table of contents

### Classes

- [Data](classes/data.md)

### Interfaces

- [DataItem](interfaces/dataitem.md)

### Variables

- [data](modules.md#data)

## Variables

### data

• `Const` **data**: [*Data*](classes/data.md)

Defined in: index.ts:158
