"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.data = exports.Data = void 0;
const fs = require("fs");
class Data {
    constructor() {
        this.path = '';
        Data.plugins = [];
    }
    setPath(path) {
        this.path = path;
    }
    static addPlugin(name) {
        Data.plugins.push(name);
    }
    static CreateID() {
        // TODO CHECK ALREADY USED ?
        return Math.random().toString(16).substr(2, 14);
    }
    static FilterItems(plugin, filter) {
        if (!exports.data[plugin])
            return undefined;
        return exports.data[plugin].filter(filter);
    }
    static FindItemById(plugin, id) {
        if (!exports.data[plugin])
            return undefined;
        return exports.data[plugin].find((item) => item.id === id);
    }
    static FindItem(plugin, check) {
        if (!exports.data[plugin])
            return undefined;
        return exports.data[plugin].find(check);
    }
    static FindItemIndex(plugin, check) {
        if (!exports.data[plugin])
            return undefined;
        return exports.data[plugin].findIndex(check);
    }
    static UpdateItem(plugin, oldItem, updates) {
        if (!exports.data[plugin])
            return undefined;
        let id = (typeof oldItem === 'string' ? oldItem : oldItem.id);
        let newItem = Data.FindItemById(plugin, id);
        if (!newItem)
            return undefined;
        Object.keys(updates).forEach(key => {
            newItem[key] = updates[key];
        });
        newItem['updatedAt'] = new Date();
        let index = Data.FindItemIndex(plugin, item => item.id === id);
        Data.RemoveItem(plugin, index);
        exports.data[plugin].push(newItem);
        exports.data.save();
        return newItem;
    }
    static SetItem(plugin, content) {
        if (!exports.data[plugin])
            return undefined;
        if (exports.data[plugin].length !== 0) {
            Data.RemoveItem(plugin, 0);
        }
        Data.CreateItem(plugin, content);
    }
    static CreateItem(plugin, content) {
        if (!exports.data[plugin])
            return undefined;
        let item = Object.assign({ id: Data.CreateID(), createdAt: new Date() }, content);
        exports.data[plugin].push(item);
        exports.data.save();
        return item;
    }
    static RemoveItem(plugin, index) {
        if (!exports.data[plugin])
            return false;
        if (index >= 0 && index < exports.data[plugin].length) {
            exports.data[plugin].splice(index, 1);
            exports.data.save();
            return true;
        }
        return false;
    }
    load() {
        if (this.path === '')
            return false;
        if (fs.existsSync(this.path)) {
            let rawdata = fs.readFileSync(this.path, 'utf8');
            let content = JSON.parse(rawdata);
            Data.plugins.forEach(plugin => {
                if (Object.keys(content).includes(plugin)) {
                    this[plugin] = content[plugin];
                }
            });
            return true;
        }
        return false;
    }
    save() {
        if (this.path === '')
            return false;
        let content = {};
        Data.plugins.forEach(plugin => {
            content[plugin] = this[plugin];
        });
        fs.writeFileSync(this.path, JSON.stringify(content));
        return true;
    }
}
exports.Data = Data;
exports.data = new Data();
