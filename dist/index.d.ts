export interface DataItem {
    id?: string;
    createdAt?: Date;
}
export declare class Data {
    private path;
    static plugins: string[];
    constructor();
    setPath(path: string): void;
    static addPlugin(name: string): void;
    static CreateID(): string;
    static FilterItems<T extends DataItem>(plugin: string, filter: (item: T) => boolean): T[];
    static FindItemById<T extends DataItem>(plugin: string, id: string): T;
    static FindItem<T extends DataItem>(plugin: string, check: (item: T) => boolean): T;
    static FindItemIndex<T extends DataItem>(plugin: string, check: (item: T) => boolean): number;
    static UpdateItem<T extends DataItem>(plugin: string, oldItem: T | string, updates: any): T;
    static SetItem<T extends DataItem>(plugin: string, content: T): void;
    static CreateItem<T extends DataItem>(plugin: string, content: T): T;
    static RemoveItem(plugin: string, index: number): boolean;
    load(): boolean;
    save(): boolean;
}
export declare const data: Data;
